
A partir de un proyecto de laravel en la version 5.5

```bash
composer create-project laravel/laravel test "5.5.*"
```

Generar una migracion para la entidad **Role** con la siguiente data

```php
$roles=[
    ['id' => 1, 'name' => 'SUPER_ADMIN'],
    ['id' => 2, 'name' => 'ADMIN'],
    ['id' => 3, 'name' => 'USER'],
];
```
Crear un proveedor de servicios **RolesFacadeServiceProvider**
que utilice una fachada **RolesFacade**
que mediante un servicio **RolesService** devuelva los valores de la tabla **roles** ordenados por id descendente

En la ruta **/roles** imprimir en formato json los roles

```php

Route::get('/roles', function() {
   return RolesFacade::roles();
});

```
**Respuesta**
```json
[
  {
    "id": 3,
    "name": "USER"
  },
  {
    "id": 2,
    "name": "ADMIN"
  },
  {
    "id": 1,
    "name": "SUPER_ADMIN"
  }
]
```

Crear un nuevo proyecto en un repositorio ej: bitbucket o gitlab y compartirlo
